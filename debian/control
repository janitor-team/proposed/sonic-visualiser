Source: sonic-visualiser
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 qt5-qmake,
 qtbase5-dev,
 qttools5-dev,
 qttools5-dev-tools,
 libqt5svg5-dev,
 libbz2-dev,
 capnproto,
 libcapnp-dev (>= 0.6.0),
 libdataquay-dev (>= 0.9-3),
 libfftw3-dev,
 libfishsound1-dev,
 libid3tag0-dev,
 libjack-dev,
 liblo-dev,
 liblrdf0-dev,
 libopusfile-dev,
 libmad0-dev,
 liboggz2-dev,
 libpulse-dev,
 librdf0-dev,
 librubberband-dev,
 libsamplerate0-dev,
 libsm-dev,
 libsndfile1-dev,
 libsord-dev,
 libx11-dev,
 portaudio19-dev,
 vamp-plugin-sdk (>= 2.5),
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://www.sonicvisualiser.org
Vcs-Git: https://salsa.debian.org/multimedia-team/sonic-visualiser.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/sonic-visualiser

Package: sonic-visualiser
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: viewing and analysing the contents of music audio files
 The aim of Sonic Visualiser is to be the first program you reach for
 when want to study a musical recording rather than simply listen to it.
 .
 Sonic Visualiser could be of particular interest to musicologists,
 archivists, signal-processing researchers and anyone else looking for a
 friendly way to take a look at what lies inside the audio file.
